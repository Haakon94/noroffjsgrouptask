**Experis Academy, Norway**
# NoroffJSGroupTask


**Authors:**
* **Håkon, Stian, Ørjan**

# Experis Week6 GroupTask
## Task

Running version of the porgram is hosted on Heroku, and can be found [here](https://journalapp69.herokuapp.com/).

If running the program locally, use these shell-commands:


## Install

```bash
$ npm install
```

## Usage

Setup db;
```bash
$ npx sequelize-cli db:migrate
```


For running the program.
```bash
$ npm run dev
```



Known bugs / TODO:
- [x] TODO: logout functionality
- [ ] Add username of logged in person to Nav-bar
- [x] Program crashes when a non-registered user attempts to log in
- [x] A user with username "" (blank) can be created