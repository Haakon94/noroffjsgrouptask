'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('journals', 'userId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'users',
        key: 'id'
      } 

    });

  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('journals', 'userId');
  }
};
