'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('journals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      subtitle: {
        type: Sequelize.STRING
      },
      body: {
        type: Sequelize.STRING
      },// kanskje eg må kjøre migrations
      //ja, det må vi uansett for å oppdatere dben
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }


       
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('journals');
  }
};