'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('journals', [{
        title: 'seed title',
        subtitle: 'seed subtitle',
        body: 'seed body',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'seed title2',
        subtitle: 'seed subtitle2',
        body: 'seed body2',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('journals', null, {});
  }
};
