
var express = require('express');
var path = require('path');
var app = express();
const Sequelize = require('sequelize');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

let sequelize;
if (process.env.DATABASE_URL) {
  sequelize = new Sequelize(process.env.DATABASE_URL);
} else {
  sequelize = new Sequelize('development', '69lover', '6969', {
    host: 'localhost',
    dialect: 'postgres'
  });
}

const Journal = sequelize.define('journal', {
  title: Sequelize.STRING,
  subtitle: Sequelize.STRING,
  body: Sequelize.STRING,
  userId: {
    type: Sequelize.INTEGER,
    references: 'users',
    referencesKey: 'id'
  }
}, {});


const User = sequelize.define('user', {
  username: Sequelize.STRING,
  password: Sequelize.STRING,
}, {});

User.hasMany(Journal);

var bodyParser = require('body-parser');
app.use(bodyParser.json());



/*
// Assign authdb to req and continue with next middleware
// Note: req is passed to each request the server receives
// IncomingRequest { } => ([err], req, res, next)
app.use((req, res, next) => {
  req.db = authdb
  next()
})
*/


app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: 'keyboard cat',
  cookie: {
  httpOnly: true,
  maxAge: 3600000, // 1 hour
  }
  }));


// Setup Passport
app.use(passport.initialize());
// Tell passport to use the local session
app.use(passport.session());

// Convert a user into its primary key
passport.serializeUser((user, done) => done(null, user.username || user));

// Fetch a user object from the database using the primary key
passport.deserializeUser((username, done) => done(null, { username }));



passport.use(new LocalStrategy({
  // Field names to read off incoming requests
  usernameField: 'username',
  passwordField: 'password',

  // Remember authentication using sessions using previously configured express-session
  session: true,
  passReqToCallback: true,

}, async (req, username, password, done) => {
  try {
    // Get stored bcrypt hash -- this will throw an error if not present.
    const stored = await User.findOne({
      where: {username: req.body.username}
    });


    // Asynchronously compare the the hashed password
    console.log("Inside localstrategy:");
    console.log("username: " + username);
    console.log("password: " + password);
    
    //console.log("stored: " + stored.password);


    const result = await new Promise((resolve, reject) => {
      bcrypt.compare(password, stored.password, (err, success) => {
        if (err) {
          console.log("error");
          return reject(err);
        }

        // Resolves true or false for comparison of the hash
        resolve(success)
      });
    });

    if (result) {
      console.log("result true (?) - username");
      
      return done(null, { username });

    } else {
      console.log("result false (?) - Incorrect password.");
      return done(null, false, { message: 'Incorrect password.' });
    }
  } catch (err) {
    if (err.notFound) {
      console.log("User doesn't exist")
      return done(null, false, { message: 'User does not exist.' });
    }
    console.log("error 3 - Something _really_ bad happened... if you ever get here then you messed up your code.");
    return done(err)
  }
}));



// Setup login endpoint
app.post('/login',
passport.authenticate('local'),
(req, res) => {
    User.findOne({where: {username: req.user.username}}).then(user => {
      console.log("user id: " + user.id);
      Journal.findAll({
        where: {userId: user.id},
       order: [
         ['updatedAt', 'DESC']
       ]
      }).then(journals => {
        console.log("Journals: ");
        console.log(journals);
         res.status(201).json(journals);
      });
    });
  console.log("inside app.post /login");
});


// Clear user session.
app.get('/logout',
  (req, res) => {
    req.logout()
    res.status(200).json({ status: 'ok' })
  })



// Authentication test
app.get('/secret', (req, res) => {
  if (req.isAuthenticated()) {
    res.json({username: req.user.username});
  }
});



app.post('/register', async (req, res, next) => {

  console.log(req.body);
    const existing = await User.findOne({
      where: {username: req.body.username}
    });
    console.log(existing);
    if (existing) {
      console.log("User exists");       
      // Again -- this is not a good idea from a security standpoint.
      return res.status(403).json({ status: 'error', message: 'User already exists.' });
    } else {
    // User not found -- proceed with registration
      
      console.log("User does not exist");


      // Bcrypt config
      const saltRoundsString = process.env.SALT_ROUNDS;
      const saltRounds = saltRoundsString ? Number(saltRoundsString) : 10;

      // Create password hash
      // Thou shalt not store your passwords in plaintext.
      const hash = await bcrypt.hash(req.body.password, saltRounds)

      // Store in db -- asynchronous operation
      await User.create({ 
        username: req.body.username, 
        password: hash
      });

      // Notify the user of their success. Respond with 201 Created.
      // Note user is not logged in at this point.
      return res.status(201).json({ status: 'ok', message: 'User registered. Please log in.' })
    

    // Pass error to default error handler
    return next(err)
  }
})

// --- GET ---
app.get('/api', function(req, res) {
  if(req.isAuthenticated()) {
    User.findOne({where: {username: req.user.username}}).then(user => {
      console.log("user id: " + user.id);
      Journal.findAll({
        where: {userId: user.id},
       order: [
         ['updatedAt', 'DESC']
       ]
      }).then(journals => {
        // console.log("Journals: ");
        // console.log(journals);
         res.status(201).json(journals);
      });
    });
  }
});


// --- POST ---
app.post('/api', function (req, res) {
  console.log(req.body);
  console.log("Session info:")
  console.log(req.session.id);
  console.log(req.session.cookie);

 if(req.isAuthenticated()) {
   User.findOne({where: {username: req.user.username}}).then(user => {
     Journal.create({
      title: req.body.title, 
      subtitle: req.body.subtitle, 
      body: req.body.body,
      userId: user.id
     }).then(result => {
        // console.log("\nFollowing journal created: ");
        // console.log("title: " + req.body.title);
        // console.log("subtitle: " + req.body.subtitle);
        // console.log("userId: " + user.id +"\n");       
        res.status(201).json(result);
     });
   });
 }
});


// --- PUT ---
app.put('/api', function(req, res) {
    console.log(req.body.id);

    Journal.update(
    { 
      title: req.body.title, 
      subtitle: req.body.subtitle, 
      body: req.body.body
    },
    {
      where: {id: req.body.id},
    }).then(result => {      
        res.status(201).json(result);
      });
    });


// --- DELETE ---
app.delete('/api/:id', function(req, res) {
  const { id } = req.params;
  console.log(id);
  Journal.findByPk(id)
      .then(journal => {
        return journal.destroy();
      }).then(() => {
        User.findOne({where: {username: req.user.username}}).then(user => {
          console.log("user id: " + user.id);
          Journal.findAll({
            where: {userId: user.id},
           order: [
             ['updatedAt', 'DESC']
           ]
          }).then(journals => {
            console.log("Journals: ");
            console.log(journals);
             res.status(201).json(journals);
          });
        });
      });
});

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(process.cwd(), 'App', 'build')))
}

const port = process.env.PORT || 6969;

app.listen(port, function() {
  console.log('Example app listening on a port!', port);
});

