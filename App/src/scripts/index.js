import '../styles/index.scss';
import * as moment from 'moment';

var apiUrl = 'localhost:6969';
 

var journId = 0;


function renderEntries(journalEntries) {
    $('#container_entries').empty();
    $.each(journalEntries, (i, entry) => {
        $('#container_entries').append(journalToHTML(entry));
    });
}

// Delete entry
$('#container_entries').delegate(".btn-danger", "click", function(){

    console.log("'.btn-danger' 'click'");

    let urlId = "/api/" + $(this).parent().attr('id').split('_')[1];
    
    console.log("url to delete element: " + urlId);
   $.ajax({
        type: 'DELETE',     
        url: urlId,
    }).done(data => {
        // console.log("Delete done from client side");
        // console.log(data);
        renderEntries(data);
    });
});


// Update entry
$('#container_entries').delegate(".btn_update", "click", function() {

    console.log("'.btn_update' 'click'");

    let id =$(this).parent().attr('id').split('_')[1];
    let title = $(this).parent().find(".jour_title").find("h2").text();
    let subtitle = $(this).parent().find(".jour_subtitle").find("h3").text();
    let body = $(this).parent().find(".jour_body").find("p").text();
    let createdDate = $(this).parent().find(".created_Date").find("p").text();

    console.log($(this).parent());  
      $(this).parent().html(   `  
        <div class="jour_title">
            <input class="form-control" type="text" value="${title}" id="input_updateTitle_${id}">
        </div

        <div class="jour_subtitle">
            <input class="form-control" type="text" value="${subtitle}" id="input_updateSubtitle_${id}">
        </div>

        <div class="jour_body">
            <textarea class="form-control" type="text" id="textarea_updateBody_${id}">${body}</textarea>
        </div>

        <div class="jour_date created_Date">
            <p class="jour_data" id="createdDate_${id}">${createdDate}</p>
        </div>
        
        <button class="submitSave btn btn-info" style="margin-top: 15px" id="button_saveChanges${id}">Save changes</button> 

    `);  
});

$("#container_entries").delegate(".submitSave", "click", function() {
    console.log("SaveChanges clicked");

    console.log($(this).parent());
    let id = $(this).parent().attr('id').split('_')[1];

    let title = $(this).parent().find("#input_updateTitle_"+id).val();
    let subtitle = $(this).parent().find("#input_updateSubtitle_"+id).val();
    let body = $(this).parent().find("#textarea_updateBody_"+id).val();
    let createdDate = $(this).parent().find("#createdDate_"+id).val();

    var data = {};
    data.id = id;
    data.title = title;
    data.subtitle = subtitle;
    data.createdDate = createdDate;
    data.modifiedDate = moment().format('MMMM Do YYYY, h:mm:ss a');
    data.body = body;
    $.ajax({
        type: 'PUT',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/api/',						
        success: function(data) {
            // console.log('success');
            // console.log(JSON.stringify(data));
            getJournalEntries();
        }
    }); 
});


function getJournalEntries() {
    $.ajax({
        type: 'GET',     
        url: /api/,
    }).done(data => {
        // console.log(data);
        renderEntries(data);
    });
}

// Create journal
function createJournalEntry(){
    var m = moment().format('MMMM Do YYYY, h:mm:ss a');
    var data = {};
    data.id = ++journId;
    data.title = $('#input_title').val();
    data.subtitle = $('#input_subtitle').val();
    data.createdDate = m;
    data.modifiedDate = m;
    data.body = $('#textarea_body').val();
    //data.userId = TODO?? Eller skal denne infereres fra backend utifra bruker/session?;
  
    
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/api/',						
        success: function(data) {
            // console.log('success');
            // console.log(JSON.stringify(data));
            getJournalEntries();
        }
    });
}

$(document).ready(function() {
    $(".journal_section").css("background-color", "white");
    $("#container_form").append(renderLogIn());
    $("#logOut").hide();
});
$("#container_form").delegate("#button_submit", "click", function() {
    createJournalEntry();
});

 // Register  button click
 $("#container_form").delegate("#button_register", "click", function() {
    var data = {};
    data.username = $("#input_username").val();
    data.password = $("#input_password").val();
    // console.log("User name: "+$("#input_username").val());
    // console.log("passord: " +$("#input_password").val());
    if(data.password.length <= 7){
        $(".feedback").html(`
        <p>Password is too short! \n 
        It must contain at least 8 characters</p>`);
    } else {
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/register',                        
            success: function(data) {
                // console.log('success');
                console.log(JSON.stringify(data));
                $(".feedback").html(`<p>Successful register. Please login in</p>`);
            },
            error: function(data) {
                console.log(JSON.stringify(data));
                $(".feedback").html(`<p>Failed registration. Please try again</p>`);
            }
    
        });
    }
    
});

// Log in
$("#container_form").delegate("#button_logIn", "click", function() {
    $("#logOut").show();
    
    var data = {};
    data.username = $("#input_username").val();
    data.password = $("#input_password").val();
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/login',                        
        success: function(data) {
            // console.log('success. You have been logged in');
            // console.log(JSON.stringify(data));
            // If successfull render journalform and the users journal entries
             $("#container_form").empty();
             $(".journal_section").css("background-color", "#8C9493");
             $("#stickyd-footer").css("position", "relative");
             $("#container_form").append(renderJournalForm()); 
             getJournalEntries();
            
        }
    });
});

// Log out
$("#navbar").delegate(".logout", "click", function() {
    console.log("attempting to log out");
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: '/logout',                        
        success: function() {
            console.log('success. You have been logged out');
            $("#container_form").empty();
            $("#container_entries").empty();
            $("#container_form").append(renderLogIn());
        }
    });
    $("#logOut").hide();
});

// Render the journal entry to html
function renderJournalForm() {
    return `
    <div class="form-row justify-content-center">
    <div class="col-md-8">
    <input class="form-control" type="text" placeholder="Title" id="input_title">
    <input class="form-control" type="text" placeholder="Subtitle" id="input_subtitle">
    </div>
</div>
<div class="form-row justify-content-center">
<div class="col-md-8">
    <div class="form-group">
      <label for="exampleFormControlTextarea1"></label>
      <textarea class="form-control" id="textarea_body" placeholder="Write your journal here..." rows="3"></textarea>
    </div>
    </div>
</div>
<div class="form-row justify-content-center">
<div class="col-md-8">
    <button type="submit" class="btn btn-primary" id="button_submit">Create</button>
    </div>
    </div>
    `;
}



// Render the login form to html
function renderLogIn() {
    return `
    <div class="form-row justify-content-center">
        <div class="col-md-6">
            <input class="form-control" type="text" placeholder="Username" id="input_username">
        </div>
    </div>
    <div class="form-row justify-content-center">
        <div class="col-md-6">
            <input class="form-control" type="password" placeholder="Password" id="input_password">
     </div>
    </div>
    <div class="form-row justify-content-center">
        <div class="col-md-6">
            <button class="btn btn-primary mt-3" id="button_logIn">Log in</button> 
            <button class="btn btn-primary mt-3" id="button_register">Register</button> 
        </div>
    </div>
    <div class="form-row justify-content-center">
        <div class="col-md-6">
            <div class="feedback mt-2" id="feedback"></div>
        </div>
    </div>
    `;
}

 function journalToHTML(journalEntry){
     return `
        <div class="journal mt-2" id="journal_${journalEntry.id}">

            <div class="jour_title">
                <h2>${journalEntry.title}</h2>
            </div>

            <div class="jour_subtitle">
                <h3>${journalEntry.subtitle}</h3>
            </div>

            <div class="jour_body">
                <p>${journalEntry.body}</p>
            </div>

            <div class="jour_date created_Date">
                <p class="jour_data">Created date: ${journalEntry.createdAt}</p>
            </div>

            <div class="jour_date modified_Date">
                <p>Modified date: ${journalEntry.updatedAt}</p>
            </div>

            <button class="btn btn-info btn_update">Update</button>

            <button class="btn btn-danger">Delete</button>
        </div>
     `;
 }

 global.journId = journId;